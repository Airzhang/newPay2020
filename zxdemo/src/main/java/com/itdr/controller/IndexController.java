package com.itdr.controller;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import com.itdr.utils.ZFBUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.apache.catalina.manager.Constants.CHARSET;

/**
 * ClassName: IndexController
 * 日期: 2020/11/18 15:50
 *
 * @author Air张
 * @since JDK 1.8
 */

@RestController
@RequestMapping("/zfb/")
public class IndexController {

    @Autowired
    ZFBUtil zfbUtil;

    /**
     * 电脑网站支付测试接口
     * @param httpResponse
     * @param shopName 商户名称
     * @param orderNo   订单编号
     * @param totalPrice    订单价格
     * @param returnUrl 支付成功回调地址，跟异步通知验签不是一个地址
     * @return
     * @throws IOException
     */
    @RequestMapping("payPage")
    public String payPage(HttpServletResponse httpResponse,String shopName, Long orderNo, BigDecimal totalPrice, String returnUrl){
        //使用工具类执行支付
        AlipayTradePagePayResponse alipayTradePagePayResponse = zfbUtil.payPage(shopName,orderNo,totalPrice,returnUrl);
        //返回数据，前端渲染后，跳转到支付宝支付界面
        return alipayTradePagePayResponse.getBody();
    }

    /**
     * 电脑网站支付成功后跳转接口
     * @param httpResponse
     * @param out_trade_no 商户订单号
     * @param total_amount 订单金额
     * @param timestamp 支付时间
     * @throws IOException
     */
    @RequestMapping("returnPayPage")
    public void payPageReturn(HttpServletResponse httpResponse,String out_trade_no,String total_amount,String timestamp) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("订单支付成功！");
        stringBuffer.append("订单号：");
        stringBuffer.append(out_trade_no);
        stringBuffer.append("，金额：");
        stringBuffer.append(total_amount);
        stringBuffer.append("，支付时间：");
        stringBuffer.append(timestamp);

        httpResponse.setContentType( "text/html;charset="  + CHARSET);
        httpResponse.getWriter().write(stringBuffer.toString()); //直接将完整的表单html输出到页面
        httpResponse.getWriter().write("<br><a href='http://localhost:8866/home.html'>回到首页</a>"); //跳转回首页
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
    }

    @RequestMapping("/yq")
    public void zfbYQ(HttpServletResponse response,HttpServletRequest request) throws IOException {
        // 获取支付宝请求参数集合
        Map<String, String[]> parameterMap = request.getParameterMap();
        // 处理集合中数据，符合验签标准
        Map<String, String> parMap = new HashMap<>();
        for (String s : parameterMap.keySet()) {
            String[] strings = parameterMap.get(s);
            parMap.put(s,strings[0]);
        }
        // 验签
        boolean b = zfbUtil.zfNotify(parMap);
        // 验签结果
        if(b){
            // 执行业务操作
            response.getWriter().write("success");
        }else{
            response.getWriter().write("failure");
        }
    }
}
