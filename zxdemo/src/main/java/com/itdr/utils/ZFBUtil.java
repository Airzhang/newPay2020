package com.itdr.utils;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.Config;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.facetoface.models.AlipayTradePrecreateResponse;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import com.itdr.configs.ZFBProConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: ZFBUtil
 * 日期: 2020/11/18 16:07
 *
 * @author Air张
 * @since JDK 1.8
 */
@Component
public class ZFBUtil implements ApplicationRunner {
    @Autowired
    ZFBProConfig zfbProConfig;

    /**
     * 实现ApplicationRunner接口，重写run方法，在项目启动时就执行，用来设置支付宝全局参数
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        Factory.setOptions(zfbProConfig.getOptions());
    }

    /**
     * 请求参数处理，转换成对应格式
     *
     * @param shopName
     * @param orderNo
     * @param totalPrice
     * @param returnUrl
     * @return
     */
    private Map<String, String> anyToString(String shopName, Long orderNo, BigDecimal totalPrice, String returnUrl) {
        Map<String, String> m = new HashMap<>();
        m.put("subject", shopName);
        m.put("outTradeNo", String.valueOf(orderNo));//订单号转字符串
        m.put("totalAmount", String.valueOf(totalPrice));//订单价格转字符串
        m.put("returnUrl", returnUrl);
        return m;
    }

    /**
     * 不同的支付场景，验签功能略有不同，查看对应手册
     * @param parameters
     * @return
     */
    public boolean zfNotify(Map<String, String> parameters) {
        boolean bol = false;
        try {
            bol = Factory.Payment.Common().verifyNotify(parameters);
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
        return bol;
    }

    /**
     * 电脑网站支付
     *
     * @param shopName   //商户名称
     * @param orderNo    //商户订单号
     * @param totalPrice //订单金额
     * @param returnUrl  //支付成功返回地址
     * @return
     */
    public AlipayTradePagePayResponse payPage(String shopName, Long orderNo, BigDecimal totalPrice, String returnUrl) {
        AlipayTradePagePayResponse response = null;
        Map<String, String> stringStringMap = anyToString(shopName, orderNo, totalPrice, returnUrl);
        try {
            response = Factory.Payment.Page().
                    pay(stringStringMap.get("subject"),
                            stringStringMap.get("outTradeNo"),
                            stringStringMap.get("totalAmount"),
                            stringStringMap.get("returnUrl"));
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

        return response;
    }

    public AlipayTradePrecreateResponse payFaceToFace(String payTitle) {
        AlipayTradePrecreateResponse response = null;
        try {
            response = Factory.Payment.FaceToFace()
                    .preCreate("沙箱测试", "2234567891", "0.01");
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

        return response;
    }
}
