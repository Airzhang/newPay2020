package com.itdr.configs;

import com.alipay.easysdk.kernel.Config;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * ClassName: ZFBProConfig
 * 日期: 2020/11/18 16:09
 *
 * @author Air张
 * @since JDK 1.8
 */

@Component
@ConfigurationProperties(prefix = "zfb") //支付宝配置文件中，配置项的前缀
@PropertySource("classpath:zfbinfo.properties") //读取支付宝配置文件信息
@Data
public class ZFBProConfig {
    private String protocol;
    private String gatewayHost;
    private String signType;
    private String appId;
    private String merchantPrivateKey;
    private String merchantCertPath;
    private String aliPayCertPath;
    private String aliPayRootCertPath;
    private String aliPayPublicKey;
    private String notifyUrl;
    private String encryptKey;

    /**
     * 获取支付宝配置对象，参数需要哪个，配置哪个，不需要的注释掉
     * @return
     */
    public Config getOptions() {
        Config config = new Config();
        config.protocol = protocol; //请求类型
        config.gatewayHost = gatewayHost;   //支付宝网关
        config.signType = signType;   //签名类型
        config.appId = appId;   //应用ID
        config.merchantPrivateKey = merchantPrivateKey; //应用私钥
        //注：证书文件路径支持设置为文件系统中的路径或CLASS_PATH中的路径，优先从文件系统中加载，加载失败后会继续尝试从CLASS_PATH中加载
//        config.merchantCertPath = merchantCertPath;
//        config.alipayCertPath = aliPayCertPath;
//        config.alipayRootCertPath = aliPayRootCertPath;
        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
        config.alipayPublicKey = aliPayPublicKey;
        //可设置异步通知接收服务地址（可选）
        config.notifyUrl = notifyUrl;
        //可设置AES密钥，调用AES加解密相关接口时需要（可选）
//        config.encryptKey = encryptKey;
        return config;
    }
}
