package com.itdr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZxdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZxdemoApplication.class, args);
    }

}
