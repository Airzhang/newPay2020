# newPay2020
[![star](https://gitee.com/Airzhang/newPay2020/badge/star.svg?theme=white)](https://gitee.com/Airzhang/newPay2020/stargazers)


## 介绍
[![AirZhang/newPay2020](https://gitee.com/Airzhang/newPay2020/widgets/widget_card.svg?colors=1e252b,,323d47,455059,d7deea,99a0ae)](https://gitee.com/Airzhang/newPay2020)

## 项目文档
目前没有单独文档页面，相关文档在wiki中

## 组织结构
````
newPay2020
├── gfdemo -- 官方案例演示，面对面扫码
├── zxdemo -- 新版SDK案例演示
    ├── configs -- 支付宝配置类
    ├── controller -- 测试代码
    ├── utils -- 支付宝工具类
    ├── resources -- 支付宝配置文件
├── imgs -- 案例演示图片
````
## 技术选型
#### 后端技术
| 技术 | 说明 | 官网 |
| :----- | :----- | :----- |
| SpringBoot | 容器+MVC框架 | https://spring.io/projects/spring-boot |
| Lombok | 简化对象封装工具 | https://github.com/rzwitserloot/lombok |

#### 前端技术
| 技术 | 说明 | 官网 |
| :----- | :----- | :----- |
| Vue | 前端框架 | https://vuejs.org/ |
| Vant | 轻量、可靠的移动端 Vue 组件库 | https://vant-contrib.gitee.io/vant/#/zh-CN/ |
| jQuery | 一个 JavaScript 库 | https://jquery.com/ |

## 功能介绍
+ 电脑网站支付（完成）
+ 面对面扫码支付（未完成）
+ 待添加

## 环境搭建
#### 开发工具
| 工具 | 说明 | 官网 |
| :----- | :----- | :----- |
| IDEA | 开发IDE | https://www.jetbrains.com/idea/download |
| alipaykeytool | 支付宝开放平台开发助手 | https://opendocs.alipay.com/open/291/105971 |

#### 开发环境
| 工具 | 版本号 | 下载 |
| :----- | :----- | :----- |
| JDK | 1.8     | https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html |
| alipaykeytool | 支付宝<br>开放平台<br>开发助手 | https://opendocs.alipay.com/open/291/105971 |

## 使用说明

1.  克隆项目到本地
2.  使用idea打开项目，更新依赖
3.  修改支付宝配置文件内容为对应沙箱应用中参数
4.  配置tomcat服务器，启动项目
5.  进入localhost:8866/home.html 进行测试

## 测试图片
待完善

##  许可证
[Apache License 2.0](https://github.com/macrozheng/mall/blob/master/LICENSE)
  
Copyright (c) 2018-2020 newPay2020